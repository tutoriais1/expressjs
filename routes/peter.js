const express = require('express')
const axios = require('axios')
const router = express.Router()

// middleware that is specific to this router
router.use((req, res, next) => {
  console.log('Time: ', Date.now())
  next()
})
// define the home page route
router.get('/', (req, res) => {
  res.send('First name')
})
// define the axios route
router.get('/dolar/venda', (req, res, next) => {
  axios.get("https://olinda.bcb.gov.br/olinda/servico/PTAX/versao/v1/odata/CotacaoDolarDia(dataCotacao=@dataCotacao)?@dataCotacao='04-08-2022'&$top=100&$format=json&$select=cotacaoVenda")
  .then(response => res.json(response.data))
  .catch(err => next(err));
  })  
module.exports = router


